//
//  AddItemViewController.swift
//  CollectionView
//
//  Created by amr on 3/3/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit
protocol AddItemsVCDelegate {
   
    func Additems(_controller : AddItemViewController,didFinishAdding item : String)
}
class AddItemViewController: UIViewController {

    @IBOutlet weak var itemNameTF: UITextField!
    var delegate : AddItemsVCDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addClicked(_ sender: Any) {
        if itemNameTF.text?.isEmpty == false {
            delegate?.Additems(_controller: self, didFinishAdding: itemNameTF.text!)
           
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
