//
//  ViewController.swift
//  CollectionView
//
//  Created by amr on 3/3/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource , AddItemsVCDelegate {
    
    @IBOutlet weak var addBTN: UIBarButtonItem!
     @IBOutlet weak var collectionView: UICollectionView!
    var collectionData = ["1 😊", "2 🏸", "3 🏋🏻‍♀️", "4 🤾‍♀️", "5 🎤", "6 🚵🏼", "7 📀", "8 🎛", "9 📻", "10 ⚔️", "11 🔦", "12 🇪🇬"]
    
    
    
    //MARK :- Collection view functions
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        cell.itemNameLbl.text = collectionData[indexPath.row]
        cell.isEditing = isEditing
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !isEditing{
            performSegue(withIdentifier: "DetailsSegue", sender: indexPath)
        }
    }
    
    //MARK:- Additemsviewcontrollerdelegate functions
    func Additems(_controller: AddItemViewController, didFinishAdding item: String) {
        
        collectionData.append(item)
        let index = IndexPath(row: collectionData.count - 1, section: 0)
        collectionView.insertItems(at: [index])
        navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK :- Prepare for segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailsSegue"
        {
        if let dest = segue.destination as? DetailsVC
            {
               let index = sender as! IndexPath
            dest.selection = collectionData[index.row]
        }
        }
        if segue.identifier == "AddSegue"
        {
            if let dest = segue.destination as? AddItemViewController {
                dest.delegate = self
            }
        }
        
    }
    //MARK : - functions
    
    @objc func refresh() {
        collectionData.append("item")
        let index = IndexPath(row: collectionData.count - 1, section: 0)
        collectionView.insertItems(at: [index])
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        addBTN.isEnabled = !editing
        collectionView.allowsMultipleSelection = editing
        let indexes  = collectionView.indexPathsForVisibleItems
        for index in indexes {
            let cell = collectionView.cellForItem(at: index) as! CollectionViewCell
            cell.isEditing = editing
        }
    }

    
    //MARK: - ViewDidLoad
   
    override func viewDidLoad() {
        super.viewDidLoad()
      collectionView.delegate = self
        collectionView.dataSource = self
        
        let width = (view.frame.size.width - 20) / 3
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: width)
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        collectionView.refreshControl = refresh
        navigationItem.leftBarButtonItem = editButtonItem
    }
    

}

