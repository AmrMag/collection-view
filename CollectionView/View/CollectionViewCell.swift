//
//  CollectionViewCell.swift
//  CollectionView
//
//  Created by amr on 3/3/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var itemNameLbl: UILabel!
    
    @IBOutlet weak var checkImg: UIImageView!

    var isEditing: Bool = false{
        didSet {
             checkImg.isHidden = !isEditing
        }
    }
    override var isSelected: Bool {
        didSet {
            if isEditing {
                checkImg.image = isSelected ? UIImage(named : "Checked") : UIImage(named: "Unchecked")
            }
        }
    }
}
